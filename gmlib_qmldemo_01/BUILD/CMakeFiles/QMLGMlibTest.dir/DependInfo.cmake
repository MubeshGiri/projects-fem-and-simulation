# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "C:/programming/simulation/glcontextsurfacewrapper.cpp" "C:/programming/gmlib_qmldemo_01/BUILD/CMakeFiles/QMLGMlibTest.dir/glcontextsurfacewrapper.cpp.obj"
  "C:/programming/simulation/glscenerenderer.cpp" "C:/programming/gmlib_qmldemo_01/BUILD/CMakeFiles/QMLGMlibTest.dir/glscenerenderer.cpp.obj"
  "C:/programming/simulation/gmlibwrapper.cpp" "C:/programming/gmlib_qmldemo_01/BUILD/CMakeFiles/QMLGMlibTest.dir/gmlibwrapper.cpp.obj"
  "C:/programming/simulation/guiapplication.cpp" "C:/programming/gmlib_qmldemo_01/BUILD/CMakeFiles/QMLGMlibTest.dir/guiapplication.cpp.obj"
  "C:/programming/simulation/main.cpp" "C:/programming/gmlib_qmldemo_01/BUILD/CMakeFiles/QMLGMlibTest.dir/main.cpp.obj"
  "C:/programming/gmlib_qmldemo_01/BUILD/moc_glscenerenderer.cpp" "C:/programming/gmlib_qmldemo_01/BUILD/CMakeFiles/QMLGMlibTest.dir/moc_glscenerenderer.cpp.obj"
  "C:/programming/gmlib_qmldemo_01/BUILD/moc_gmlibwrapper.cpp" "C:/programming/gmlib_qmldemo_01/BUILD/CMakeFiles/QMLGMlibTest.dir/moc_gmlibwrapper.cpp.obj"
  "C:/programming/gmlib_qmldemo_01/BUILD/moc_guiapplication.cpp" "C:/programming/gmlib_qmldemo_01/BUILD/CMakeFiles/QMLGMlibTest.dir/moc_guiapplication.cpp.obj"
  "C:/programming/gmlib_qmldemo_01/BUILD/moc_window.cpp" "C:/programming/gmlib_qmldemo_01/BUILD/CMakeFiles/QMLGMlibTest.dir/moc_window.cpp.obj"
  "C:/programming/gmlib_qmldemo_01/BUILD/qrc_qml.cpp" "C:/programming/gmlib_qmldemo_01/BUILD/CMakeFiles/QMLGMlibTest.dir/qrc_qml.cpp.obj"
  "C:/programming/simulation/window.cpp" "C:/programming/gmlib_qmldemo_01/BUILD/CMakeFiles/QMLGMlibTest.dir/window.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GLEW_STATIC"
  "QT_CORE_LIB"
  "QT_GUI_LIB"
  "QT_NETWORK_LIB"
  "QT_NO_DEBUG"
  "QT_QML_LIB"
  "QT_QUICK_LIB"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "C:/Qt/5.4/mingw491_32/include"
  "C:/Qt/5.4/mingw491_32/include/QtCore"
  "C:/Qt/5.4/mingw491_32/mkspecs/win32-g++"
  "C:/Qt/5.4/mingw491_32/include/QtQuick"
  "C:/Qt/5.4/mingw491_32/include/QtGui"
  "C:/Qt/5.4/mingw491_32/include/QtQml"
  "C:/Qt/5.4/mingw491_32/include/QtNetwork"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )
